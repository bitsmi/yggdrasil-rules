package com.bitsmi.yggdrasil.rules;

import java.util.ArrayList;
import java.util.List;

public class RuleSetImpl implements IRuleSet
{
	private List<IRuleSetRuleDescriptor> rules = new ArrayList<>();
	
	public List<IRuleSetRuleDescriptor> getRules()
	{
		return rules;
	}
	
	RuleSetImpl setRules(List<IRuleSetRuleDescriptor> rules)
	{
		this.rules = rules;
		return this;
	}
}
