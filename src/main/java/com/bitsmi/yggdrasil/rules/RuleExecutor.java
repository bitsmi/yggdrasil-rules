package com.bitsmi.yggdrasil.rules;

import com.bitsmi.yggdrasil.commons.ReflectiveCallHelper;
import com.bitsmi.yggdrasil.commons.ReflectiveCallHelper.ReflectiveCallInstance;

public class RuleExecutor 
{
	RuleExecutor() 
	{
		
	}
	
	RuleExecution executeRule(IRuleSetRuleDescriptor ruleDescriptor, IRuleContext context)
	{
		return executeRule(ruleDescriptor, context, null);
	}
	
	RuleExecution executeRule(IRuleSetRuleDescriptor ruleDescriptor, IRuleContext context, Integer executionIndex)
	{
		RuleExecution executionData = new RuleExecution();
		executionData.setRuleId(ruleDescriptor.getRuleId());
		executionData.setExecutionIndex(executionIndex);
		if(RuleSetClassRuleDescriptor.RULE_TYPE_CLASS.equals(ruleDescriptor.getRuleType())) {
			RuleSetClassRuleDescriptor<?, ?, ?> concreteDescriptor = (RuleSetClassRuleDescriptor<?, ?, ?>)ruleDescriptor;
			ReflectiveCallInstance<? extends IRule<?, ?>> ruleInstace = ReflectiveCallHelper
					.of(concreteDescriptor.getRuleClass())
					.instantiateDefault();
			Boolean executeRule = false;
			try {
				executeRule = ruleInstace.call("when", new Class[] {IRuleContext.class}, context);
			}
			catch(ReflectiveOperationException e) {
				executionData.addError(new RuleExecution.ExecutionError()
						.setMessage("Error evaluating 'When' expression")
						.setCause(e));
			}
			
			executionData.setExecuted(executeRule);
			if(executeRule) {
				try {
					Object result = ruleInstace.call("then", new Class[] {IRuleContext.class}, context);
					executionData.setResult(result);
				}
				catch(ReflectiveOperationException e) {
					executionData.addError(new RuleExecution.ExecutionError()
							.setMessage("Error executing 'Then' clause")
							.setCause(e));
				}
			}
		}
		else if(RuleSetProgrammaticRuleDescriptor.RULE_TYPE_PROGRAMMATIC.equals(ruleDescriptor.getRuleType())) {
			RuleSetProgrammaticRuleDescriptor<IRuleContext, ?> concreteDescriptor = (RuleSetProgrammaticRuleDescriptor<IRuleContext, ?>)ruleDescriptor;
			boolean executeRule = false;
			try {
				executeRule = concreteDescriptor.getWhenPredicate().test(context);
			}
			catch(RuntimeException e) {
				executionData.addError(new RuleExecution.ExecutionError()
						.setMessage("Error evaluating 'When' expression")
						.setCause(e));
			}
			executionData.setExecuted(executeRule);
			if(executeRule) {
				try {
					Object result = concreteDescriptor.getThenFunction().apply(context);
					executionData.setResult(result);
				}
				catch(RuntimeException e) {
					executionData.addError(new RuleExecution.ExecutionError()
							.setMessage("Error executing 'Then' clause")
							.setCause(e));
				}
			}
		}
		
		return executionData;
	}
}
