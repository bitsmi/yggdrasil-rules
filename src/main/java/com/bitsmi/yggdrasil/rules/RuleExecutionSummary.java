package com.bitsmi.yggdrasil.rules;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class RuleExecutionSummary 
{
	private List<RuleExecution> ruleExecutions = new ArrayList<>();

	public List<RuleExecution> getAllRuleExecutions() 
	{
		return ruleExecutions;
	}
	
	public List<RuleExecution> getExecutedRuleExecutions()
	{
		return ruleExecutions.stream()
				.filter(re -> re.isExecuted())
				.collect(Collectors.toList());
	}
	
	public boolean hasExecutedRuleExecutions()
	{
		return ruleExecutions.stream()
				.anyMatch(re -> re.isExecuted());
	}
	
	public List<RuleExecution> getNonExecutedRuleExecutions()
	{
		return ruleExecutions.stream()
				.filter(re -> !re.isExecuted())
				.collect(Collectors.toList());
	}
	
	public List<RuleExecution> getSuccededRuleExecutions()
	{
		return ruleExecutions.stream()
				.filter(re -> re.isExecuted() && !re.hasErrors())
				.collect(Collectors.toList());
	}
	
	public boolean hasSuccededRuleExecutions()
	{
		return ruleExecutions.stream()
				.anyMatch(re -> re.isExecuted() && !re.hasErrors());
	}
	
	public List<RuleExecution> getFailedRuleExecutions()
	{
		return ruleExecutions.stream()
				.filter(re -> re.hasErrors())
				.collect(Collectors.toList());
	}
	
	public boolean hasFailedExecutions() 
	{
		return ruleExecutions.stream()
				.anyMatch(re -> re.hasErrors());
	}
	
	public List<RuleExecution> getRuleExecutions(String ruleId)
	{
		Objects.requireNonNull(ruleId);
		
		return ruleExecutions.stream()
				.filter(re -> ruleId.equals(re.getRuleId()))
				.collect(Collectors.toList());
	}

	void addRuleExecution(RuleExecution ruleExecution)
	{
		this.ruleExecutions.add(ruleExecution);
	}
	
	void combine(RuleExecutionSummary summary)
	{
		this.ruleExecutions.addAll(summary.ruleExecutions);
	}
}
