package com.bitsmi.yggdrasil.rules;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.bitsmi.yggdrasil.commons.functional.UncheckedFunctionals;

public class SequentialRuleSetExecutor implements IRuleSetExecutorService 
{
	private RuleExecutor ruleExecutor;
	
	SequentialRuleSetExecutor()
	{
		this.ruleExecutor = new RuleExecutor();
	}
	
	@Override
	public <T> T executeAllRules(IRuleContext context, IRuleSet ruleSet, Collector<RuleExecution, ?, T> collector) throws RuleSetExecutionException
	{
		return executeAllRules(context, ruleSet, collector, false);
	}
	
	@Override
	public <T> T executeAllRulesIgnoringErrors(IRuleContext context, IRuleSet ruleSet, Collector<RuleExecution, ?, T> collector)
	{
		T result = null;
		try{
			result = executeAllRules(context, ruleSet, collector, true);
		}
		catch(RuleSetExecutionException e) {
			// Cannot happen...
		}
		return result;
	}
	
	private <T> T executeAllRules(IRuleContext context, IRuleSet ruleSet, Collector<RuleExecution, ?, T> collector, boolean ignoreErrors)  throws RuleSetExecutionException
	{
		Objects.requireNonNull(context);
		Objects.requireNonNull(ruleSet);
		Objects.requireNonNull(collector);
		
		int[] executionIndex = {1};
		List<RuleExecution> ruleExecutions = ruleSet.getRules()
				.stream()
				// Filter rules with the specified context
				.filter(rule -> context.getClass().isAssignableFrom(rule.getContextClass()))
				// Execute all rules and collect results
				.map(rule -> ruleExecutor.executeRule(rule, context, executionIndex[0]++))
				.collect(Collectors.toList());
		
		if(!ignoreErrors) {
			List<RuleExecution> failedExecutions = ruleExecutions.stream()
					.filter(RuleExecution::hasErrors)
					.collect(Collectors.toList());
			
			throw new RuleSetExecutionException(failedExecutions);
		}
		
		return ruleExecutions.stream()
				.collect(collector);
	}
	
	@Override
	public Optional<RuleExecution> executeFirstMatchingRule(IRuleContext context, IRuleSet ruleSet) throws RuleSetExecutionException
	{
		Objects.requireNonNull(context);
		Objects.requireNonNull(ruleSet);
		
		int[] executionIndex = {1};
		Optional<RuleExecution> optRuleExecution = ruleSet.getRules()
				.stream()
				// Filter rules with the specified context
				.filter(rule -> context.getClass().isAssignableFrom(rule.getContextClass()))
				// Execute rules until one of them produce a valid result
				.map(rule -> ruleExecutor.executeRule(rule, context, executionIndex[0]++))
				.peek(UncheckedFunctionals.uncheckedConsumer(exec -> {
					if(exec.hasErrors()) {
						throw new RuleSetExecutionException(exec);
					}
				}))
				.filter(exec -> exec.isExecuted())
				.findFirst();
		
		return optRuleExecution;
	}
	
	@Override
	public Optional<RuleExecution> executeFirstMatchingRuleIgnoringErrors(IRuleContext context, IRuleSet ruleSet)
	{
		Objects.requireNonNull(context);
		Objects.requireNonNull(ruleSet);
		
		int[] executionIndex = {1};
		Optional<RuleExecution> optRuleExecution = ruleSet.getRules()
				.stream()
				// Filter rules with the specified context
				.filter(rule -> context.getClass().isAssignableFrom(rule.getContextClass()))
				// Execute rules until one of them produce a valid result
				.map(rule -> ruleExecutor.executeRule(rule, context, executionIndex[0]++))
				.filter(exec -> exec.isExecuted() && !exec.hasErrors())
				.findFirst();
		
		return optRuleExecution;
	}
}
