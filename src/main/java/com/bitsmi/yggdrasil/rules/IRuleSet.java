package com.bitsmi.yggdrasil.rules;

import java.util.List;

public interface IRuleSet
{
	public List<IRuleSetRuleDescriptor> getRules();
}
