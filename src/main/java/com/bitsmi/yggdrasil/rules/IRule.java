package com.bitsmi.yggdrasil.rules;

public interface IRule <C extends IRuleContext, R>
{
	public Class<C> given();
	public boolean when(C context);
	public R then(C context);
}
