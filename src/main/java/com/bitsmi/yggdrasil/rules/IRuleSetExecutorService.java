package com.bitsmi.yggdrasil.rules;

import java.util.Optional;
import java.util.stream.Collector;

public interface IRuleSetExecutorService 
{
	public <T> T executeAllRules(IRuleContext context, IRuleSet ruleSet, Collector<RuleExecution, ?, T> collector) throws RuleSetExecutionException;
	public <T> T executeAllRulesIgnoringErrors(IRuleContext context, IRuleSet ruleSet, Collector<RuleExecution, ?, T> collector);
	
	public Optional<RuleExecution> executeFirstMatchingRule(IRuleContext context, IRuleSet ruleSet) throws RuleSetExecutionException;
	public Optional<RuleExecution> executeFirstMatchingRuleIgnoringErrors(IRuleContext context, IRuleSet ruleSet);
}
