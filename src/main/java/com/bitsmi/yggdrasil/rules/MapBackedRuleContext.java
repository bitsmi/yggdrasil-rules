package com.bitsmi.yggdrasil.rules;

import java.util.HashMap;
import java.util.Map;

public class MapBackedRuleContext implements IRuleContext 
{
	private Map<String, Object> values = new HashMap<>();
	
	public Object getValue(String key)
	{
		return values.get(key);
	}
	
	public boolean isPresent(String key)
	{
		return values.containsKey(key);
	}
	
	public void setValue(String key, Object value)
	{
		this.values.put(key, value);
	}
}