package com.bitsmi.yggdrasil.rules;

import java.util.function.Function;
import java.util.function.Predicate;

public class RuleSetProgrammaticRuleDescriptor<C extends IRuleContext, R> implements IRuleSetRuleDescriptor 
{
	public static final String RULE_TYPE_PROGRAMMATIC = "PROGRAMMATIC";
	
	private String ruleId;
	private Class<C> contextClass;
	private Class<R> resultClass;
	
	private Predicate<C> whenPredicate;
	private Function<C, R> thenFunction;
	
	@Override
	public String getRuleId() 
	{
		return ruleId;
	}
	
	RuleSetProgrammaticRuleDescriptor<C, R> setRuleId(String ruleId)
	{
		this.ruleId = ruleId;
		return this;
	}
	
	@Override
	public String getRuleType()
	{
		return RULE_TYPE_PROGRAMMATIC;
	}
	
	@Override
	public Class<C> getContextClass()
	{
		return contextClass;
	}
	
	RuleSetProgrammaticRuleDescriptor<C, R> setContextClass(Class<C> contextClass) 
	{
		this.contextClass = contextClass;
		return this;
	}
	
	public Class<R> getResultClass() 
	{
		return resultClass;
	}

	RuleSetProgrammaticRuleDescriptor<C, R> setResultClass(Class<R> resultClass) 
	{
		this.resultClass = resultClass;
		return this;
	}

	public Predicate<C> getWhenPredicate() 
	{
		return whenPredicate;
	}

	void setWhenPredicate(Predicate<C> predicate) 
	{
		this.whenPredicate = predicate;
	}

	public Function<C, R> getThenFunction() 
	{
		return thenFunction;
	}

	void setThenFunction(Function<C, R> function) 
	{
		this.thenFunction = function;
	}
}
