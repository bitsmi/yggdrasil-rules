package com.bitsmi.yggdrasil.rules;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class ResultCollectors 
{
	public static Collector<RuleExecution, ?, RuleExecutionSummary> toRuleExecutionSummary()
	{
		return new SummaryCollector();
	}
	
	public static Collector<RuleExecution, ?, Integer> sumAsInt()
	{
		return Collectors.summingInt(ResultCollectors::resultToInt);
	}
	
	public static Collector<RuleExecution, ?, List<RuleExecution>> toRuleExecutionList()
	{
		return Collectors.toList();				
	}
	
	/*--------------------------------*
     * SUPPORT METHODS AND CLASSES
     *--------------------------------*/
	static final int resultToInt(RuleExecution ruleExecution)
	{
		if(!ruleExecution.isExecuted()
				|| ruleExecution.hasErrors()
				|| ruleExecution.getResult()==null) {
			return 0;
		}
		
		Object result = ruleExecution.getResult();
		if(result instanceof Integer) {
			return (Integer)result;
		}
		else if(result instanceof Number) {
			return ((Number)result).intValue();
		}
		else if(result instanceof String && isNumeric((String)result)) {
			return Integer.parseInt((String)result);
		}
		else {
			throw new IllegalArgumentException("Invalid Integer value (" + result + ")");
		}
	}
	
	static boolean isNumeric(final CharSequence cs) 
	{
		if(cs==null || cs.length()==0) {
			return false;
		}
		
        for (int i = 0; i < cs.length(); i++) {
            if (!Character.isDigit(cs.charAt(i))) {
                return false;
            }
        }
        return true;
    }
	
	static class SummaryCollector implements Collector<RuleExecution, RuleExecutionSummary, RuleExecutionSummary>
	{
		@Override
		public Set<Characteristics> characteristics() 
		{
			Set<Characteristics> characteristics = new HashSet<>();
			characteristics.add(Characteristics.UNORDERED);
			
			return characteristics;
		}
		
		@Override
		public Supplier<RuleExecutionSummary> supplier() 
		{
			return RuleExecutionSummary::new;
		}

		@Override
		public BiConsumer<RuleExecutionSummary, RuleExecution> accumulator() 
		{
			return RuleExecutionSummary::addRuleExecution;
		}

		@Override
		public BinaryOperator<RuleExecutionSummary> combiner() 
		{
			return (r1, r2) -> {
				r1.combine(r2);
				return r1;
			};
		}

		@Override
		public Function<RuleExecutionSummary, RuleExecutionSummary> finisher() 
		{
			return Function.identity();
		}
	}
}
