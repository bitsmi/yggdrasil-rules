package com.bitsmi.yggdrasil.rules;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class RuleSetExecutionException extends Exception 
{
	private static final long serialVersionUID = -1037142789544801889L;
	
	private Collection<RuleExecution> failedExecutions;
	
	public RuleSetExecutionException(RuleExecution failedExecution)
	{
		this.failedExecutions = new ArrayList<>();
		this.failedExecutions.add(failedExecution);
	}
	
	public RuleSetExecutionException(Collection<RuleExecution> failedExecutions)
	{
		this.failedExecutions = failedExecutions;
	}
	
	@Override
	public String getMessage()
	{
		return "Failed to execute rules";
	}
	
	public Collection<RuleExecution> getFailedExecutions()
	{
		if(failedExecutions==null) {
			return Collections.emptyList();
		}
		return failedExecutions;
	}
}
