package com.bitsmi.yggdrasil.rules;

public class RuleSetClassRuleDescriptor <U extends IRule<C, R>, C extends IRuleContext, R> implements IRuleSetRuleDescriptor 
{
	public static final String RULE_TYPE_CLASS = "CLASS";
	
	private String ruleId;
	private Class<U> ruleClass;
	private Class<C> contextClass;
	private Class<R> resultClass;

	@Override
	public String getRuleId() 
	{
		return ruleId;
	}
	
	RuleSetClassRuleDescriptor<U, C, R> setRuleId(String ruleId)
	{
		this.ruleId = ruleId;
		return this;
	}
	
	@Override
	public String getRuleType()
	{
		return RULE_TYPE_CLASS;
	}
	
	@Override
	public Class<C> getContextClass()
	{
		return contextClass;
	}
	
	RuleSetClassRuleDescriptor<U, C, R> setContextClass(Class<C> contextClass) 
	{
		this.contextClass = contextClass;
		return this;
	}
	
	public Class<U> getRuleClass() 
	{
		return ruleClass;
	}

	RuleSetClassRuleDescriptor<U, C, R> setRuleClass(Class<U> ruleClass) 
	{
		this.ruleClass = ruleClass;
		return this;
	}

	public Class<R> getResultClass() 
	{
		return resultClass;
	}

	RuleSetClassRuleDescriptor<U, C, R> setResultClass(Class<R> resultClass) 
	{
		this.resultClass = resultClass;
		return this;
	}
}
