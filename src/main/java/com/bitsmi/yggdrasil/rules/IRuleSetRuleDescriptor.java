package com.bitsmi.yggdrasil.rules;

public interface IRuleSetRuleDescriptor 
{
	public String getRuleId();
	public String getRuleType();
	public Class<?> getContextClass();
}
