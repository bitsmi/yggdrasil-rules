package com.bitsmi.yggdrasil.rules;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class RuleExecution 
{
	private String ruleId;
	private Integer executionIndex;
	private boolean executed;
	private List<ExecutionError> errors;
	private Object result;
	
	RuleExecution() 
	{
		
	}
	
	public String getRuleId()
	{
		return ruleId;
	}
	
	RuleExecution setRuleId(String ruleId)
	{
		this.ruleId = ruleId;
		return this;
	}
	
	public Integer getExecutionIndex()
	{
		return executionIndex;
	}
	
	RuleExecution setExecutionIndex(Integer executionIndex)
	{
		this.executionIndex = executionIndex;
		return this;
	}
	
	public Object getResult()
	{
		return result;
	}
	
	RuleExecution setResult(Object result)
	{
		this.result = result;
		return this;
	}

	public boolean isExecuted()
	{
		return executed;
	}
	
	RuleExecution setExecuted(boolean executed)
	{
		this.executed = executed;
		return this;
	}
	
	public boolean hasErrors() 
	{
		return errors!=null && !errors.isEmpty();
	}
	
	public List<ExecutionError> getErrors()
	{
		if(errors==null) {
			return Collections.emptyList();
		}
		return errors;
	}
	
	RuleExecution setErrors(List<ExecutionError> errors)
	{
		this.errors = errors;
		return this;
	}
	
	RuleExecution addError(ExecutionError error)
	{
		if(this.errors==null) {
			this.errors = new ArrayList<>();
		}
		this.errors.add(error);
		return this;
	}
	
	RuleExecution addErrors(Collection<ExecutionError> errors)
	{
		if(this.errors==null) {
			this.errors = new ArrayList<>();
		}
		this.errors.addAll(errors);
		return this;
	}
	
	@Override
	public String toString() 
	{
		StringBuilder builder = new StringBuilder();
		builder.append("RuleExecution [ruleId=").append(ruleId)
				.append(", executionIndex=").append(executionIndex)
				.append(", executed=").append(executed)
				.append(", errors=").append(errors)
				.append(", result=").append(result).append("]");
		return builder.toString();
	}

	/*--------------------------------*
     * SUPPORT METHODS AND CLASSES
     *--------------------------------*/
	public static class ExecutionError
	{
		private String message;
		private Exception cause;
		
		public String getMessage() 
		{
			return message;
		}
		
		ExecutionError setMessage(String message) 
		{
			this.message = message;
			return this;
		}
		
		public Exception getCause() 
		{
			return cause;
		}
		
		ExecutionError setCause(Exception cause) 
		{
			this.cause = cause;
			return this;
		}

		@Override
		public String toString() 
		{
			StringBuilder builder = new StringBuilder();
			builder.append("ExecutionError [message=").append(message)
					.append(", cause=").append(cause).append("]");
			return builder.toString();
		}
	}
}
