package com.bitsmi.yggdrasil.rules;

public class RuleSetExecutors 
{
	public static IRuleSetExecutorService newSequentialExecutor()
	{
		return new SequentialRuleSetExecutor();
	}
}
