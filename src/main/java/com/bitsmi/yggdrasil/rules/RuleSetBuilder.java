package com.bitsmi.yggdrasil.rules;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.google.common.reflect.TypeToken;

public class RuleSetBuilder
{
	private List<IRuleSetRuleDescriptor> rules = new ArrayList<>();
	
	@SuppressWarnings({ "unchecked" })
	public <U extends IRule<C, R>, C extends IRuleContext, R> RuleSetBuilder addRule(Class<U> ruleClass)
    {
		Optional<Class<?>> optClass = flattenClassHierarchy(ruleClass) 
				.flatMap(c -> Stream.of(c.getInterfaces()))
				.filter(c -> IRule.class.getName().equals(c.getName()))
				.findFirst();
		
		if(!optClass.isPresent()) {
			throw new IllegalArgumentException(ruleClass.getName() + " is not a valid Rule");
		}

		TypeToken<U> ruleToken = TypeToken.of(ruleClass);
		TypeToken<?> ruleContextToken = ruleToken.resolveType(IRule.class.getTypeParameters()[0]);
		TypeToken<?> ruleResultToken = ruleToken.resolveType(IRule.class.getTypeParameters()[1]);
		
		Class<C> contextClass = (Class<C>)ruleContextToken.getType();
		Class<R> resultClass = (Class<R>)ruleResultToken.getType();
		
		RuleSetClassRuleDescriptor<U, C, R> descriptor = new RuleSetClassRuleDescriptor<U, C, R>()
				.setContextClass(contextClass)
				.setRuleClass(ruleClass)
				.setResultClass(resultClass);
		
		return addRule(descriptor);
    }
	
	public <C extends IRuleContext, R> RuleSetBuilder createRule(Class<C> contextClass, Class<R> resultClass, FunctionalRuleBuilder<C, R> ruleBuilder)
    {
		RuleSetProgrammaticRuleDescriptor<C, R> descriptor = new RuleSetProgrammaticRuleDescriptor<C, R>()
				.setContextClass(contextClass)
				.setResultClass(resultClass);
		
		return addRule(descriptor, ruleBuilder);
    }
	
	private <U extends IRule<C, R>, C extends IRuleContext, R> RuleSetBuilder addRule(RuleSetClassRuleDescriptor<U, C, R> descriptor)
	{
		rules.add(descriptor);
		
		descriptor.setRuleId(descriptor.getRuleClass().getName());
		
        return this;
    }
	
	private <C extends IRuleContext, R> RuleSetBuilder addRule(RuleSetProgrammaticRuleDescriptor<C, R> descriptor, FunctionalRuleBuilder<C, R> ruleBuilder)
	{
		RuleBuilder<C, R> builder = new RuleBuilder<>();
		ruleBuilder.build(builder);
		
		descriptor.setWhenPredicate(builder.getWhenPredicate());
		descriptor.setThenFunction(builder.getThenFunction());
		
		rules.add(descriptor);
		
		String ruleId = builder.getId();
		if(ruleId==null) {
			ruleId = UUID.randomUUID().toString();
		}
		descriptor.setRuleId(ruleId);
		
        return this;
    }
	
	public IRuleSet build()
	{
		RuleSetImpl ruleSet = new RuleSetImpl()
				.setRules(rules);
		
		return ruleSet;
	}
	
	public static class RuleBuilder<C, R>
	{
		private String id;
		private Predicate<C> whenPredicate;
		private Function<C, R> thenFunction;
		
		public String getId()
		{
			return id;
		}
		
		public RuleBuilder<C, R> setId(String id) 
		{
			this.id = id;
			return this;
		}
		
		public Predicate<C> getWhenPredicate() 
		{
			return whenPredicate;
		}
		
		public RuleBuilder<C, R> when(Predicate<C> predicate) 
		{
			this.whenPredicate = predicate;
			return this;
		}
		
		public Function<C, R> getThenFunction() 
		{
			return thenFunction;
		}
		
		public RuleBuilder<C, R> then(Function<C, R> function) 
		{
			this.thenFunction = function;
			return this;
		}
	}
	
	public static interface FunctionalRuleBuilder<C, R>
	{
		public void build(RuleBuilder<C, R> builder);
	}
	
	/*--------------------------------*
     * SUPPORT METHODS AND CLASSES
     *--------------------------------*/
	private Stream<Class<?>> flattenClassHierarchy(Class<?> clazz)
	{
		if(clazz.getSuperclass()==null) {
			return Stream.empty();
		}
		
		return Stream.concat(Stream.of(clazz), 
					flattenClassHierarchy(clazz.getSuperclass())); 
	}
}
