package com.bitsmi.yggdrasil.rules.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import com.bitsmi.yggdrasil.rules.IRuleSet;
import com.bitsmi.yggdrasil.rules.IRuleSetExecutorService;
import com.bitsmi.yggdrasil.rules.MapBackedRuleContext;
import com.bitsmi.yggdrasil.rules.ResultCollectors;
import com.bitsmi.yggdrasil.rules.RuleExecution;
import com.bitsmi.yggdrasil.rules.RuleSetBuilder;
import com.bitsmi.yggdrasil.rules.RuleSetExecutionException;
import com.bitsmi.yggdrasil.rules.RuleSetExecutors;

public class SequentialRuleSetExecutorTestCase 
{
	/*--------------------------------*
     * EXECUTE ALL RULES
     *--------------------------------*/
	@ParameterizedTest(name = "Test {0}")
	@MethodSource("createSuccessRuleSetValues")
	public void allRulesExecutionTest(String testName, IRuleSet ruleSet)
	{
		MapBackedRuleContext context = new MapBackedRuleContext();
		context.setValue(TestSupport.IntegerResultTestRule1.CTX_THEN_CLAUSE_RESULT, 1);
		context.setValue(TestSupport.IntegerResultTestRule2.CTX_THEN_CLAUSE_RESULT, 2);
		
		IRuleSetExecutorService executorService = RuleSetExecutors.newSequentialExecutor();
		
		// Execute both rules
		context.setValue(TestSupport.IntegerResultTestRule1.CTX_WHEN_CLAUSE_RESULT, Boolean.TRUE);
		context.setValue(TestSupport.IntegerResultTestRule2.CTX_WHEN_CLAUSE_RESULT, Boolean.TRUE);
		context.setValue("WHEN_CLAUSE_1", Boolean.TRUE);
		context.setValue("WHEN_CLAUSE_2", Boolean.TRUE);
		List<RuleExecution> result1 = executorService.executeAllRulesIgnoringErrors(context, ruleSet, ResultCollectors.toRuleExecutionList());
		// Execute only Rule1
		context.setValue(TestSupport.IntegerResultTestRule2.CTX_WHEN_CLAUSE_RESULT, Boolean.FALSE);
		context.setValue("WHEN_CLAUSE_2", Boolean.FALSE);
		List<RuleExecution> result2 = executorService.executeAllRulesIgnoringErrors(context, ruleSet, ResultCollectors.toRuleExecutionList());
		
		List<Predicate<RuleExecution>> predicates1 = Arrays.asList(
                this.testRuleExecution(true, 1, 0, 1),
                this.testRuleExecution(true, 2, 0, 2));
		List<Predicate<RuleExecution>> predicates2 = Arrays.asList(
                this.testRuleExecution(true, 1, 0, 1),
                this.testRuleExecution(false, 2, 0, null));
        
        assertListItems(result1, predicates1);
        assertListItems(result2, predicates2);
	}
	
	@ParameterizedTest(name = "Test {0}")
	@MethodSource("createFailingRuleSetValues")
	public void allRulesFailingWhenClauseTest(String testName, IRuleSet ruleSet)
	{
		MapBackedRuleContext context = new MapBackedRuleContext();
		context.setValue(TestSupport.IntegerResultTestRule1.CTX_THEN_CLAUSE_RESULT, 1);
		
		IRuleSetExecutorService executorService = RuleSetExecutors.newSequentialExecutor();
		
		// Fail on When clause
		context.setValue(TestSupport.FailingTestRule.CTX_FAIL_ON_WHEN_CLAUSE, Boolean.TRUE);
		context.setValue("FAIL_ON_WHEN_CLAUSE", Boolean.TRUE);
		Throwable thrown = catchThrowable(() -> {
			executorService.executeAllRules(context, ruleSet, ResultCollectors.toRuleExecutionList());
		});
		
		assertThat(thrown).isInstanceOf(RuleSetExecutionException.class);
		assertThat(((RuleSetExecutionException)thrown).getFailedExecutions())
				.hasSize(2)
				.allMatch(execution -> execution.hasErrors()
						&& execution.getErrors().size()==1
						&& "Error evaluating 'When' expression".equals(execution.getErrors().get(0).getMessage()));
	}
	
	@ParameterizedTest(name = "Test {0}")
	@MethodSource("createFailingRuleSetValues")
	public void allRulesFailingThenClauseTest(String testName, IRuleSet ruleSet)
	{
		MapBackedRuleContext context = new MapBackedRuleContext();
		context.setValue(TestSupport.IntegerResultTestRule1.CTX_THEN_CLAUSE_RESULT, 1);
		
		IRuleSetExecutorService executorService = RuleSetExecutors.newSequentialExecutor();
		
		// Fail on When clause
		context.setValue(TestSupport.FailingTestRule.CTX_FAIL_ON_THEN_CLAUSE, Boolean.TRUE);
		context.setValue("FAIL_ON_THEN_CLAUSE", Boolean.TRUE);
		Throwable thrown = catchThrowable(() -> {
			executorService.executeAllRules(context, ruleSet, ResultCollectors.toRuleExecutionList());
		});
		
		assertThat(thrown).isInstanceOf(RuleSetExecutionException.class);
		assertThat(((RuleSetExecutionException)thrown).getFailedExecutions())
				.hasSize(2)
				.allMatch(execution -> execution.hasErrors()
						&& execution.getErrors().size()==1
						&& "Error executing 'Then' clause".equals(execution.getErrors().get(0).getMessage()));
	}
	
	@ParameterizedTest(name = "Test {0}")
	@MethodSource("createFailingRuleSetValues")
	public void allRulesFailingWhenClauseIgnoreErrorsTest(String testName, IRuleSet ruleSet)
	{
		MapBackedRuleContext context = new MapBackedRuleContext();
		context.setValue(TestSupport.IntegerResultTestRule1.CTX_WHEN_CLAUSE_RESULT, true);
		context.setValue(TestSupport.IntegerResultTestRule1.CTX_THEN_CLAUSE_RESULT, 2);
		context.setValue(TestSupport.FailingTestRule.CTX_FAIL_ON_WHEN_CLAUSE, Boolean.TRUE);
		context.setValue("WHEN_CLAUSE_2", Boolean.TRUE);
		context.setValue("FAIL_ON_WHEN_CLAUSE", Boolean.TRUE);
		
		IRuleSetExecutorService executorService = RuleSetExecutors.newSequentialExecutor();
		List<RuleExecution> result = executorService.executeAllRulesIgnoringErrors(context, ruleSet, ResultCollectors.toRuleExecutionList());
		
		List<Predicate<RuleExecution>> predicates = Arrays.asList(
                this.testRuleExecution(false, 1, 1, null),
                this.testRuleExecution(true, 2, 0, 2),
                this.testRuleExecution(false, 3, 1, null));
        
        assertListItems(result, predicates);
	}
	
	@ParameterizedTest(name = "Test {0}")
	@MethodSource("createFailingRuleSetValues")
	public void allRulesFailingThenClauseIgnoreErrorsTest(String testName, IRuleSet ruleSet)
	{
		MapBackedRuleContext context = new MapBackedRuleContext();
		context.setValue(TestSupport.IntegerResultTestRule1.CTX_WHEN_CLAUSE_RESULT, true);
		context.setValue(TestSupport.IntegerResultTestRule1.CTX_THEN_CLAUSE_RESULT, 2);
		context.setValue(TestSupport.FailingTestRule.CTX_FAIL_ON_THEN_CLAUSE, Boolean.TRUE);
		context.setValue("WHEN_CLAUSE_2", Boolean.TRUE);
		context.setValue("FAIL_ON_THEN_CLAUSE", Boolean.TRUE);
		
		IRuleSetExecutorService executorService = RuleSetExecutors.newSequentialExecutor();
		List<RuleExecution> result = executorService.executeAllRulesIgnoringErrors(context, ruleSet, ResultCollectors.toRuleExecutionList());
		
		List<Predicate<RuleExecution>> predicates = Arrays.asList(
                this.testRuleExecution(true, 1, 1, null),
                this.testRuleExecution(true, 2, 0, 2),
                this.testRuleExecution(true, 3, 1, null));
        
        assertListItems(result, predicates);
	}
	
	/*--------------------------------*
     * EXECUTE FIRST MATCHING RULE
     *--------------------------------*/
	@ParameterizedTest(name = "Test {0}")
	@MethodSource("createSuccessRuleSetValues")
	public void firstMatchingRuleExecutionTest(String testName, IRuleSet ruleSet)
	{
		MapBackedRuleContext context = new MapBackedRuleContext();
		context.setValue(TestSupport.IntegerResultTestRule1.CTX_THEN_CLAUSE_RESULT, 1);
		context.setValue(TestSupport.IntegerResultTestRule2.CTX_THEN_CLAUSE_RESULT, 2);
		
		IRuleSetExecutorService executorService = RuleSetExecutors.newSequentialExecutor();
		
		// Execute both rules
		context.setValue(TestSupport.IntegerResultTestRule1.CTX_WHEN_CLAUSE_RESULT, Boolean.TRUE);
		context.setValue(TestSupport.IntegerResultTestRule2.CTX_WHEN_CLAUSE_RESULT, Boolean.TRUE);
		context.setValue("WHEN_CLAUSE_1", Boolean.TRUE);
		context.setValue("WHEN_CLAUSE_2", Boolean.TRUE);
		Optional<RuleExecution> optResult1 = executorService.executeFirstMatchingRuleIgnoringErrors(context, ruleSet);
		// Execute only Rule1
		context.setValue(TestSupport.IntegerResultTestRule1.CTX_WHEN_CLAUSE_RESULT, Boolean.FALSE);
		context.setValue("WHEN_CLAUSE_1", Boolean.FALSE);
		Optional<RuleExecution> optResult2 = executorService.executeFirstMatchingRuleIgnoringErrors(context, ruleSet);
		
		assertThat(optResult1).isPresent().hasValueSatisfying(r -> {
			assertThat(r.getRuleId()).isNotNull();
			assertThat(r.getExecutionIndex()).isEqualTo(1);
			assertThat(r.getResult()).isEqualTo(1);
		});
		assertThat(optResult2).isPresent().hasValueSatisfying(r -> {
			assertThat(r.getRuleId()).isNotNull();
			assertThat(r.getExecutionIndex()).isEqualTo(2);
			assertThat(r.getResult()).isEqualTo(2);
		});
	}
	
	@ParameterizedTest(name = "Test {0}")
	@MethodSource("createFailingRuleSetValues")
	public void firstMatchingRuleFailingWhenClauseTest(String testName, IRuleSet ruleSet)
	{
		MapBackedRuleContext context = new MapBackedRuleContext();
		context.setValue(TestSupport.IntegerResultTestRule1.CTX_THEN_CLAUSE_RESULT, 2);
		
		IRuleSetExecutorService executorService = RuleSetExecutors.newSequentialExecutor();
		
		// Fail on When clause
		context.setValue(TestSupport.FailingTestRule.CTX_FAIL_ON_WHEN_CLAUSE, Boolean.TRUE);
		context.setValue("FAIL_ON_WHEN_CLAUSE", Boolean.TRUE);
		Throwable thrown = catchThrowable(() -> {
			executorService.executeFirstMatchingRule(context, ruleSet);
		});
		
		assertThat(thrown).isInstanceOf(RuleSetExecutionException.class);
		assertThat(((RuleSetExecutionException)thrown).getFailedExecutions())
				.hasSize(1)
				.allMatch(execution -> execution.hasErrors()
						&& execution.getErrors().size()==1
						&& "Error evaluating 'When' expression".equals(execution.getErrors().get(0).getMessage()));
	}
	
	@ParameterizedTest(name = "Test {0}")
	@MethodSource("createFailingRuleSetValues")
	public void firstMatchingRuleFailingThenClauseTest(String testName, IRuleSet ruleSet)
	{
		MapBackedRuleContext context = new MapBackedRuleContext();
		context.setValue(TestSupport.IntegerResultTestRule1.CTX_THEN_CLAUSE_RESULT, 2);
		
		IRuleSetExecutorService executorService = RuleSetExecutors.newSequentialExecutor();
		
		// Fail on When clause
		context.setValue(TestSupport.FailingTestRule.CTX_FAIL_ON_THEN_CLAUSE, Boolean.TRUE);
		context.setValue("FAIL_ON_THEN_CLAUSE", Boolean.TRUE);
		Throwable thrown = catchThrowable(() -> {
			executorService.executeFirstMatchingRule(context, ruleSet);
		});
		
		assertThat(thrown).isInstanceOf(RuleSetExecutionException.class);
		assertThat(((RuleSetExecutionException)thrown).getFailedExecutions())
				.hasSize(1)
				.allMatch(execution -> execution.hasErrors()
						&& execution.getErrors().size()==1
						&& "Error executing 'Then' clause".equals(execution.getErrors().get(0).getMessage()));
	}
	
	@ParameterizedTest(name = "Test {0}")
	@MethodSource("createFailingRuleSetValues")
	public void firstMatchingRuleFailingWhenClauseIgnoreErrorsTest(String testName, IRuleSet ruleSet)
	{
		MapBackedRuleContext context = new MapBackedRuleContext();
		context.setValue(TestSupport.IntegerResultTestRule1.CTX_WHEN_CLAUSE_RESULT, Boolean.TRUE);
		context.setValue(TestSupport.IntegerResultTestRule1.CTX_THEN_CLAUSE_RESULT, 2);
		context.setValue("WHEN_CLAUSE_2", Boolean.TRUE);
		
		IRuleSetExecutorService executorService = RuleSetExecutors.newSequentialExecutor();
		
		// Fail on When clause
		context.setValue(TestSupport.FailingTestRule.CTX_FAIL_ON_WHEN_CLAUSE, Boolean.TRUE);
		context.setValue("FAIL_ON_WHEN_CLAUSE", Boolean.TRUE);
		Optional<RuleExecution> optResult = executorService.executeFirstMatchingRuleIgnoringErrors(context, ruleSet);
		
		assertThat(optResult).isPresent().hasValueSatisfying(r -> {
			assertThat(r.getRuleId()).isNotNull();
			assertThat(r.getExecutionIndex()).isEqualTo(2);
			assertThat(r.getResult()).isEqualTo(2);
		});
	}
	
	@ParameterizedTest(name = "Test {0}")
	@MethodSource("createFailingRuleSetValues")
	public void firstMatchingRuleFailingThenClauseIgnoreErrorsTest(String testName, IRuleSet ruleSet)
	{
		MapBackedRuleContext context = new MapBackedRuleContext();
		context.setValue(TestSupport.IntegerResultTestRule1.CTX_WHEN_CLAUSE_RESULT, Boolean.TRUE);
		context.setValue(TestSupport.IntegerResultTestRule1.CTX_THEN_CLAUSE_RESULT, 2);
		context.setValue("WHEN_CLAUSE_2", Boolean.TRUE);
		
		IRuleSetExecutorService executorService = RuleSetExecutors.newSequentialExecutor();
		
		// Fail on When clause
		context.setValue(TestSupport.FailingTestRule.CTX_FAIL_ON_THEN_CLAUSE, Boolean.TRUE);
		context.setValue("FAIL_ON_THEN_CLAUSE", Boolean.TRUE);
		Optional<RuleExecution> optResult = executorService.executeFirstMatchingRuleIgnoringErrors(context, ruleSet);
		
		assertThat(optResult).isPresent().hasValueSatisfying(r -> {
			assertThat(r.getRuleId()).isNotNull();
			assertThat(r.getExecutionIndex()).isEqualTo(2);
			assertThat(r.getResult()).isEqualTo(2);
		});
	}
	
	/*--------------------------------*
     * SUPPORT METHODS AND CLASSES
     *--------------------------------*/
	private static Stream<Object> createSuccessRuleSetValues()
	{
		return Stream.of(new Object[] {"Pojo ResultSet", 
					new RuleSetBuilder()
						.addRule(TestSupport.IntegerResultTestRule1.class)
						.addRule(TestSupport.IntegerResultTestRule2.class)
						.build()
				},
				new Object[] {"Programmatic ResultSet", 
						new RuleSetBuilder()
							.createRule(MapBackedRuleContext.class, Integer.class, builder -> {
								builder.when(ctx -> ctx.isPresent("WHEN_CLAUSE_1") && (Boolean)ctx.getValue("WHEN_CLAUSE_1") )
										.then(ctx -> { return 1; });
								
							})
							.createRule(MapBackedRuleContext.class, Integer.class, builder -> {
								builder.when(ctx -> ctx.isPresent("WHEN_CLAUSE_2") && (Boolean)ctx.getValue("WHEN_CLAUSE_2") )
										.then(ctx -> { return 2; });
							})
							.build()
				});
	}
	
	private static Stream<Object> createFailingRuleSetValues()
	{
		return Stream.of(new Object[] {"Pojo ResultSet", 
					new RuleSetBuilder()
						.addRule(TestSupport.FailingTestRule.class)
						.addRule(TestSupport.IntegerResultTestRule1.class)
						.addRule(TestSupport.FailingTestRule.class)
						.build()
				},
				new Object[] {"Programmatic ResultSet", 
						new RuleSetBuilder()
							.createRule(MapBackedRuleContext.class, Integer.class, builder -> {
								builder.when(ctx -> { 
										if(ctx.isPresent("FAIL_ON_WHEN_CLAUSE") && (Boolean)ctx.getValue("FAIL_ON_WHEN_CLAUSE")) {
											throw new RuntimeException("FAIL");
										}
										return true;
									})
									.then(ctx -> { 
										if(ctx.isPresent("FAIL_ON_THEN_CLAUSE") && (Boolean)ctx.getValue("FAIL_ON_THEN_CLAUSE")) {
											throw new RuntimeException("FAIL");
										}
										return 1;
									});
								
							})
							.createRule(MapBackedRuleContext.class, Integer.class, builder -> {
								builder.when(ctx -> ctx.isPresent("WHEN_CLAUSE_2") && (Boolean)ctx.getValue("WHEN_CLAUSE_2") )
									.then(ctx -> { return 2; });
							})
							.createRule(MapBackedRuleContext.class, Integer.class, builder -> {
								builder.when(ctx -> { 
										if(ctx.isPresent("FAIL_ON_WHEN_CLAUSE") && (Boolean)ctx.getValue("FAIL_ON_WHEN_CLAUSE")) {
											throw new RuntimeException("FAIL");
										}
										return true;
									})
									.then(ctx -> { 
										if(ctx.isPresent("FAIL_ON_THEN_CLAUSE") && (Boolean)ctx.getValue("FAIL_ON_THEN_CLAUSE")) {
											throw new RuntimeException("FAIL");
										}
										return 3;
									});
								
							})
							.build()
				});
	}
	
	private static <T> void assertListItems(List<T> list, List<Predicate<T>> predicates)
    {
        assertThat(list)
            .hasSize(predicates.size());
        
        int i = 0;
        for (T elem : list) {
            Predicate<T> predicate = predicates.get(i);
            assertThat(elem)
                .matches(predicate, "Item assertion: " + elem.toString());
            i++;
        }
    }
    
    private Predicate<RuleExecution> testRuleExecution(boolean isExecuted, int executionIndex, int errorCount, Object result)
    {
        return execution -> {
        	return (result!=null && result.equals(execution.getResult()) 
        				|| result==null && execution.getResult()==null)
        			&& execution.getRuleId()!=null
        			&& !(isExecuted ^ execution.isExecuted())
        			&& (!(errorCount==0 ^ !execution.hasErrors())
        				|| !(errorCount>execution.getErrors().size() ^ execution.hasErrors()));
        };
    }
}
