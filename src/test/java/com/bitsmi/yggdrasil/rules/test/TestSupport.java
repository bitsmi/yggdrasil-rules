package com.bitsmi.yggdrasil.rules.test;

import java.util.function.Function;
import java.util.function.Predicate;

import com.bitsmi.yggdrasil.rules.IRule;
import com.bitsmi.yggdrasil.rules.MapBackedRuleContext;

public class TestSupport 
{
	/*--------------------------------*
     * TEST RULE CONTEXTS
     *--------------------------------*/
	
	/*--------------------------------*
     * TEST RULES
     *--------------------------------*/
	public static abstract class GenericTestRule<R> implements IRule<MapBackedRuleContext, R>
    {
		protected abstract Predicate<MapBackedRuleContext> evaluateWhenClause();
		protected abstract Function<MapBackedRuleContext, R> executeThenClause();
		
		@Override
		public Class<MapBackedRuleContext> given()
		{
			return MapBackedRuleContext.class;
		}
		
    	@Override
        public boolean when(MapBackedRuleContext ctx)
        {
        	return evaluateWhenClause().test(ctx);
        }
    	
    	@Override
    	public R then(MapBackedRuleContext ctx)
    	{
    		return executeThenClause().apply(ctx);
    	}
    }
	
	public static class VoidTestRule extends GenericTestRule<Void>
    {
		public static final String CTX_WHEN_CLAUSE_RESULT = "VoidTestRule.when.result";
		public static final String CTX_THEN_CLAUSE_RESULT = "VoidTestRule.then.result";

		@Override
		protected Predicate<MapBackedRuleContext> evaluateWhenClause() 
		{
			return (ctx) -> ctx.isPresent(CTX_WHEN_CLAUSE_RESULT) && (Boolean)ctx.getValue(CTX_WHEN_CLAUSE_RESULT);
		}

		@Override
		protected Function<MapBackedRuleContext, Void> executeThenClause() 
		{
			return (ctx) -> null;
		}
    }
	
	public static class IntegerResultTestRule1 extends GenericTestRule<Integer> 
	{
		public static final String CTX_WHEN_CLAUSE_RESULT = "IntegerResultTestRule1.when.result";
		public static final String CTX_THEN_CLAUSE_RESULT = "IntegerResultTestRule1.then.result";

		@Override
		protected Predicate<MapBackedRuleContext> evaluateWhenClause() 
		{
			return (ctx) -> ctx.isPresent(CTX_WHEN_CLAUSE_RESULT) && (Boolean)ctx.getValue(CTX_WHEN_CLAUSE_RESULT);
		}

		@Override
		protected Function<MapBackedRuleContext, Integer> executeThenClause() 
		{
			return (ctx) -> ctx.isPresent(CTX_THEN_CLAUSE_RESULT) ? (Integer)ctx.getValue(CTX_THEN_CLAUSE_RESULT) : 0;
		}
	}
	
	public static class IntegerResultTestRule2 extends GenericTestRule<Integer> 
	{
		public static final String CTX_WHEN_CLAUSE_RESULT = "IntegerResultTestRule2.when.result";
		public static final String CTX_THEN_CLAUSE_RESULT = "IntegerResultTestRule2.then.result";

		@Override
		protected Predicate<MapBackedRuleContext> evaluateWhenClause() 
		{
			return (ctx) -> ctx.isPresent(CTX_WHEN_CLAUSE_RESULT) && (Boolean)ctx.getValue(CTX_WHEN_CLAUSE_RESULT);
		}

		@Override
		protected Function<MapBackedRuleContext, Integer> executeThenClause() 
		{
			return (ctx) -> ctx.isPresent(CTX_THEN_CLAUSE_RESULT) ? (Integer)ctx.getValue(CTX_THEN_CLAUSE_RESULT) : 0;
		}
	}
	
	public static class StringResultTestRule1 extends GenericTestRule<String> 
	{
		public static final String CTX_WHEN_CLAUSE_RESULT = "StringResultTestRule1.when.result";
		public static final String CTX_THEN_CLAUSE_RESULT = "StringResultTestRule1.then.result";

		@Override
		protected Predicate<MapBackedRuleContext> evaluateWhenClause() 
		{
			return (ctx) -> ctx.isPresent(CTX_WHEN_CLAUSE_RESULT) && (Boolean)ctx.getValue(CTX_WHEN_CLAUSE_RESULT);
		}

		@Override
		protected Function<MapBackedRuleContext, String> executeThenClause() 
		{
			return (ctx) -> (String)ctx.getValue(CTX_THEN_CLAUSE_RESULT);
		}
	}
	
	public static class StringResultTestRule2 extends GenericTestRule<String> 
	{
		public static final String CTX_WHEN_CLAUSE_RESULT = "StringResultTestRule2.when.result";
		public static final String CTX_THEN_CLAUSE_RESULT = "StringResultTestRule2.then.result";

		@Override
		protected Predicate<MapBackedRuleContext> evaluateWhenClause() 
		{
			return (ctx) -> ctx.isPresent(CTX_WHEN_CLAUSE_RESULT) && (Boolean)ctx.getValue(CTX_WHEN_CLAUSE_RESULT);
		}

		@Override
		protected Function<MapBackedRuleContext, String> executeThenClause() 
		{
			return (ctx) -> (String)ctx.getValue(CTX_THEN_CLAUSE_RESULT);
		}
	}
	
	public static class FailingTestRule extends GenericTestRule<String>
	{
		public static final String CTX_FAIL_ON_WHEN_CLAUSE = "FailingWhenTestRule.when.fail";
		public static final String CTX_FAIL_ON_THEN_CLAUSE = "FailingWhenTestRule.then.fail";

		@Override
		protected Predicate<MapBackedRuleContext> evaluateWhenClause() 
		{
			return (ctx) -> {
				if(ctx.isPresent(CTX_FAIL_ON_WHEN_CLAUSE) && (Boolean)ctx.getValue(CTX_FAIL_ON_WHEN_CLAUSE)) {
					throw new RuntimeException("Fail");
				}
				return true;
			};
		}

		@Override
		protected Function<MapBackedRuleContext, String> executeThenClause() 
		{
			return (ctx) -> {
				if(ctx.isPresent(CTX_FAIL_ON_THEN_CLAUSE) && (Boolean)ctx.getValue(CTX_FAIL_ON_THEN_CLAUSE)) {
					throw new RuntimeException("Fail");
				}
				return "Success";
			};
		}
	}
}
