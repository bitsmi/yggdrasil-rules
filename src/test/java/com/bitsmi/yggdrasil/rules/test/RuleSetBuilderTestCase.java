package com.bitsmi.yggdrasil.rules.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.bitsmi.yggdrasil.rules.RuleSetClassRuleDescriptor;
import com.bitsmi.yggdrasil.rules.IRuleSet;
import com.bitsmi.yggdrasil.rules.MapBackedRuleContext;
import com.bitsmi.yggdrasil.rules.RuleSetProgrammaticRuleDescriptor;
import com.bitsmi.yggdrasil.rules.RuleSetBuilder;

public class RuleSetBuilderTestCase 
{
	@Test
	public void buildPojoRuleSetTest()
	{
		IRuleSet ruleSet = new RuleSetBuilder()
				.addRule(TestSupport.VoidTestRule.class)
				.addRule(TestSupport.IntegerResultTestRule1.class)
				.build();
		
		assertThat(ruleSet.getRules()).hasSize(2)
				.allSatisfy(def -> {
					assertThat(def).isNotNull().isInstanceOf(RuleSetClassRuleDescriptor.class);
					assertThat(def.getRuleType()).isEqualTo(RuleSetClassRuleDescriptor.RULE_TYPE_CLASS);
					assertThat(((RuleSetClassRuleDescriptor<?, ?,?>)def).getRuleClass())
						.satisfies(clazz -> {
							assertThat(clazz.getName()).isIn(TestSupport.VoidTestRule.class.getName(), TestSupport.IntegerResultTestRule1.class.getName());
						});
					assertThat(((RuleSetClassRuleDescriptor<?, ?,?>)def).getContextClass())
						.satisfies(clazz -> {
							assertThat(clazz.getName()).isEqualTo(MapBackedRuleContext.class.getName());
					});
					assertThat(((RuleSetClassRuleDescriptor<?, ?,?>)def).getResultClass())
						.satisfies(clazz -> {
							assertThat(clazz.getName()).isIn(Void.class.getName(), Integer.class.getName());
					});
				});
		// Assert IDs
		assertThat(ruleSet.getRules().get(0).getRuleId()).isEqualTo(TestSupport.VoidTestRule.class.getName());
		assertThat(ruleSet.getRules().get(1).getRuleId()).isEqualTo(TestSupport.IntegerResultTestRule1.class.getName());
	}
	
	@Test
	public void buildProgrammaticRuleSetTest()
	{
		IRuleSet ruleSet = new RuleSetBuilder()
				.createRule(MapBackedRuleContext.class, Void.class, builder -> {
					builder.when(ctx -> { return true; })
							.then(ctx -> { return null;	});
					
				})
				.createRule(MapBackedRuleContext.class, Integer.class, builder -> {
					builder.setId("PROGRAMMATIC_RULE")
							.when(ctx -> { return true; })
							.then(ctx -> { return 1;	});
				})
				.build();
		
		assertThat(ruleSet.getRules()).hasSize(2)
				.allSatisfy(def -> {
					assertThat(def).isNotNull().isInstanceOf(RuleSetProgrammaticRuleDescriptor.class);
					assertThat(def.getRuleType()).isEqualTo(RuleSetProgrammaticRuleDescriptor.RULE_TYPE_PROGRAMMATIC);
					assertThat(((RuleSetProgrammaticRuleDescriptor<?, ?>)def).getResultClass())
						.satisfies(clazz -> {
							assertThat(clazz.getName()).isIn(Void.class.getName(), Integer.class.getName());
					});
					assertThat(((RuleSetProgrammaticRuleDescriptor<?, ?>)def).getContextClass())
						.satisfies(clazz -> {
							assertThat(clazz.getName()).isIn(MapBackedRuleContext.class.getName());
					});
					assertThat(((RuleSetProgrammaticRuleDescriptor<MapBackedRuleContext, ?>)def).getWhenPredicate())
						.satisfies(p -> {
							assertThat(p).accepts(new MapBackedRuleContext());
					});
					assertThat(((RuleSetProgrammaticRuleDescriptor<MapBackedRuleContext, ?>)def).getThenFunction())
						.satisfies(f -> {
							assertThat(f.apply(new MapBackedRuleContext())).isIn(null, 1);
					});
				});
		// Assert IDs
		assertThat(ruleSet.getRules().get(0).getRuleId()).isNotNull();
		assertThat(ruleSet.getRules().get(1).getRuleId()).isEqualTo("PROGRAMMATIC_RULE");
	}
}
