package com.bitsmi.yggdrasil.rules.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import com.bitsmi.yggdrasil.rules.PackageTestSupport;
import com.bitsmi.yggdrasil.rules.ResultCollectors;
import com.bitsmi.yggdrasil.rules.RuleExecution;
import com.bitsmi.yggdrasil.rules.RuleExecutionSummary;

public class RuleExecutionSummaryCollectorTestCase 
{
	@ParameterizedTest(name = "Test {0}")
	@MethodSource("com.bitsmi.yggdrasil.rules.PackageTestSupport#createIntegerRuleExecutionValues")
	public void integerRulesSummaryCollectorTest(String testName, List<RuleExecution> executions)
	{
		RuleExecutionSummary result = executions.stream()
				.collect(ResultCollectors.toRuleExecutionSummary());
		
		assertResult(testName, result);
	}
	
	/*------------------------------*
	 * SUPPORT METHODS AND CLASSES
	 *------------------------------*/
	public static void assertResult(String testName, RuleExecutionSummary result)
	{
		if(PackageTestSupport.TEST_NAME_INTEGER_1.equals(testName)) {
			assertThat(result.getAllRuleExecutions()).hasSize(5);
			assertThat(result.getExecutedRuleExecutions()).hasSize(3);
			assertThat(result.getFailedRuleExecutions()).hasSize(0);
			assertThat(result.getNonExecutedRuleExecutions()).hasSize(2);
			assertThat(result.getSuccededRuleExecutions()).hasSize(3);
			assertThat(result.getRuleExecutions("test.integer1.2")).hasSize(1).allSatisfy(re -> {
				assertThat(re.getRuleId()).isEqualTo("test.integer1.2");
				assertThat(re.isExecuted()).isEqualTo(Boolean.TRUE);
				assertThat(re.getResult()).isEqualTo("2");
			});
		}
		else if(PackageTestSupport.TEST_NAME_INTEGER_2.equals(testName)) {
			assertThat(result.getAllRuleExecutions()).hasSize(5);
			assertThat(result.getExecutedRuleExecutions()).hasSize(4);
			assertThat(result.getFailedRuleExecutions()).hasSize(2);
			assertThat(result.getNonExecutedRuleExecutions()).hasSize(1);
			assertThat(result.getSuccededRuleExecutions()).hasSize(3);
			assertThat(result.getRuleExecutions("test.integer2.2")).hasSize(1).allSatisfy(re -> {
				assertThat(re.getRuleId()).isEqualTo("test.integer2.2");
				assertThat(re.isExecuted()).isEqualTo(Boolean.FALSE);
				assertThat(re.hasErrors()).isEqualTo(Boolean.TRUE);
			});
		}
		else{
			Assertions.fail("Unknown test");
		}
	}
}
