package com.bitsmi.yggdrasil.rules.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import com.bitsmi.yggdrasil.rules.PackageTestSupport;
import com.bitsmi.yggdrasil.rules.ResultCollectors;
import com.bitsmi.yggdrasil.rules.RuleExecution;

public class ResultToIntCollectorTestCase 
{
	@ParameterizedTest(name = "Test {0}")
	@MethodSource("com.bitsmi.yggdrasil.rules.PackageTestSupport#createIntegerRuleExecutionValues")
	public void resultToIntCollectorTest(String testName, List<RuleExecution> executions)
	{
		Integer result = executions.stream()
				.collect(ResultCollectors.sumAsInt());
		
		assertResult(testName, result);
	}
	
	/*------------------------------*
	 * SUPPORT METHODS AND CLASSES
	 *------------------------------*/
	public static void assertResult(String testName, Integer result)
	{
		if(PackageTestSupport.TEST_NAME_INTEGER_1.equals(testName)) {
			assertThat(result).isEqualTo(3);
		}
		else if(PackageTestSupport.TEST_NAME_INTEGER_2.equals(testName)) {
			assertThat(result).isEqualTo(4);
		}
		else{
			Assertions.fail("Unknown test");
		}
	}
}
