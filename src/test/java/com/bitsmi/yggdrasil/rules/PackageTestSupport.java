package com.bitsmi.yggdrasil.rules;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PackageTestSupport 
{
	public static final String TEST_NAME_INTEGER_1 = "Test Integer 1";
	public static final String TEST_NAME_INTEGER_2 = "Test Integer 2";
	
	public static Stream<Object> createIntegerRuleExecutionValues()
	{
		return Stream.of(new Object[] {TEST_NAME_INTEGER_1, Stream.of(
						new RuleExecution().setRuleId("test.integer1.1").setExecuted(true).setResult(1),
						new RuleExecution().setRuleId("test.integer1.2").setExecuted(true).setResult("2"),
						new RuleExecution().setRuleId("test.integer1.3").setExecuted(false),
						new RuleExecution().setRuleId("test.integer1.4").setExecuted(true).setResult(null),
						new RuleExecution().setRuleId("test.integer1.5").setExecuted(false)
					)
					.collect(Collectors.toList())},
				new Object[] {TEST_NAME_INTEGER_2, Stream.of(
						new RuleExecution().setRuleId("test.integer2.1").setExecuted(true).setResult(1),
						new RuleExecution().setRuleId("test.integer2.2").setExecuted(false)
								.addError(new RuleExecution.ExecutionError().setMessage("Error evaluating 'When' expression").setCause(new RuntimeException("ERROR CAUSE"))),
						new RuleExecution().setRuleId("test.integer2.3").setExecuted(true).setResult("3"),
						new RuleExecution().setRuleId("test.integer2.4").setExecuted(true).setResult(null),
						new RuleExecution().setRuleId("test.integer2.5").setExecuted(true)
								.addError(new RuleExecution.ExecutionError().setMessage("Error executing 'Then' clause").setCause(new RuntimeException("ERROR CAUSE")))
					)
					.collect(Collectors.toList())});
	}
}
